# ansible-awx-kubernetes

This will deploy Ansible's upstream Tower project, AWX, as a distributed deployment in Kubernetes. 


## Deploy 

```
git clone https://gitlab.com/satyasure/awx-on-k8s.git
cd awx-on-k8s
kubectl apply -f .
```

After this is done deploying it takes awhile to spin up. The best way to tell when its done is by tailing the `awx-task` file and watching for this entry

```
kubectl logs -f awx-web-<some_id>

(InteractiveConsole)

```
#Git clone the offical repository of AWX (this is hosted on verified Ansible account)
$ git clone https://github.com/ansible/awx.git
$ cd awx/installer/

#If you dont have python3 installed execute this it will fail
$ head -1 inventory | sed -i 's/python3/python/g' inventory

#This file "inventory" hold all crucial configuration like passwords and user name (configurable)   
$ cat inventory |grep -v "#" |grep . 
```
    localhost ansible_connection=local ansible_python_interpreter="/usr/bin/env python3"
    [all:vars]
    dockerhub_base=ansible
    awx_task_hostname=awx
    awx_web_hostname=awxweb
    postgres_data_dir="~/.awx/pgdocker"
    host_port=80
    host_port_ssl=443
    docker_compose_dir="~/.awx/awxcompose"
    pg_username=awx
    pg_password=awxpass
    pg_database=awx
    pg_port=5432
    admin_user=admin
    admin_password=password
    create_preload_data=True
    secret_key=awxsecret
    project_data_dir=/var/lib/awx/projects
```



# Update below paramitters carefully  
```
admin_user=admin        # AWX UI admin user 
admin_password=password # AWX UI admin password 
project_data_dir=/var/lib/awx/projects # Local dir for all projects (we created this above) 
host_port=80    	# AWX UI web port
```

# END of configuration part; Play time!    

# All set now run the play from same directoy 

$ ansible-playbook -i inventory install.yml -vv

$ docker logs -f awx_task    

# Meanwhile you an access AWX web 

http://ServerIP (Host Public IP:80) wait for migrate to finish to see login page

